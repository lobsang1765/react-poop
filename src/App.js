
import './bootstrap.css';
import './App.scss';
import Home from './components/home/Home';
// import Header from './components/Header/Header'

export const App = () => {
  return (
    <div className='App'>
{/* <Header /> */}
      <Home />
    </div>
  );
}

export default App;
