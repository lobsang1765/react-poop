import React from 'react'
import style from './style.module.scss'
import { RxTwitterLogo } from 'react-icons/rx';
export default function MemeDialog() {
  return (
    <div className={style.dialog_section}>
        <div className={style.dialog_container}>
            <div className={style.meme_description}>
            From the moment I entered the world, POOP was absent from my life and I was left to be raised by cyborg dogs.
            </div>
            <div className={style.social_media}>
            <a href="https://opensea.io/collection/poop-genesis-nft/drop" target='_blank' className={style.link}>Buy Now</a>
            <a href="https://arbiscan.io//address/0x546f5ce101ad95a0cf18a71c173185ef76961ac1" target='_blank'  className={style.link}>ArbScan</a>
          
            </div>
            <div>
            <a href="https://twitter.com/Poopcoin_" target='_blank' className={style.logo}><RxTwitterLogo size={30} className={style.icon} color='white' /></a>
            
            </div>
        </div>
    </div>
  )
}
