import React, {  useEffect } from 'react'
import styles from './style.module.scss';
import HeroSection from '../HeroSection/HeroSection';
import About from '../About/About';
import Tokenomic from '../Tokenomics/Tokenomic';
import RoadMap from '../Roadmap/RoadMap';
import MemeDialog from '../MemeDialog/MemeDialog';
import { IoIosArrowUp } from 'react-icons/io';
import Header from '../Header/Header';
import Footer from '../Footer/Footer'
export default function Home() {
 
  function topFunction() {
    window.scrollTo(0, 0)
  }
  useEffect(()=> {
    window.onscroll = function () {
       scrollFunction();
     };
     function scrollFunction() {
      let mybutton = document.getElementById("myBtn");
      if (document.body.scrollTop > 20  || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
      } else {
        mybutton.style.display = "none";
      }
     }
   return ()=> {
    //remove the event listener
   }
   }, [])
  return (
    <div className={styles.home_page}>
     <button className={styles.up_button} onClick={topFunction} id="myBtn" style={{borderRadius:"50%"}}>
      <IoIosArrowUp size={30} color='white'  />
     </button>
      <Header />
      <HeroSection />
      <About />
      <Tokenomic />
      <RoadMap />
      <MemeDialog />
      <Footer />
      {/* <Om /> */}
    </div>
  )
}

