import React from 'react'
import style from './style.module.scss'
export default function RoadMap() {
  return (
    <div className={style.roadmap_section} id="roadmap">
        <div className={style.roadmap_container}>
            <div className={style.title}>
                Roadmap
            </div>
            <div className={style.hero}>
                <div className={style.left_image}>
                    <img src="/token_left.png" alt="" />
                </div>
                <div className={style.middle_card}>
                    <div>Phase 1: Meme</div>
                    <div>Phase 2: Vibe and HODL</div>
                    <div>Phase 3: Generational wealth</div>
                </div>
                <div>
                    <img src="./token_right.png" alt="" />
                </div>
            </div>
            <div className={style.jokepart}>
                <p>
                Although we don't want to reveal everything on the first day, here is a rough outline of the path ahead for $poop, with the possibility of surprises along the way. ;)
                </p>
            </div>
            <div className={style.phase_list}>
                <div className={style.card}>
                    <div>
                        <div>
                            <p>Phase 1</p>
                         
                            <ul>
                                <li>Launch</li>
                                <li>CoinGecko/Coinmarketcap Listings</li>
                                <li>1,000+ Holders</li>
                                <li>Get $POOP Trending on twitter with our memetic power</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div className={style.card}>
                    <div>
                        <div>
                        <p>Phase 2</p>
                            <ul>

                                <li>Community Partnerships </li>
                                <li>Formation of token gated discord group, for holders, more details tba</li>
                                <li>CEX Listings 10,000+holders</li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div className={style.card}>
                    <div>
                        <div>
                        <p>Phase 3</p>
                            <ul>
                                <li>$POOP themed merch, % revenues to $POOP buy and burn</li>
                                <li>$POOP Academy: details tba</li>
                                <li>$POOP Tools: details tba</li>
                                <li>T1 Exchange Listings 100,000+ holders</li>
                                <li>Flip Bitcoin</li>
                            </ul>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
  )
}
