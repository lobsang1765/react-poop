import React from 'react';
import styles from './style.module.scss'
export default function () {

  const bringTokonomic = () => {
    const element = document.getElementById('tokenomics');
    if (element) {
      // 👇 Will scroll smoothly to the top of the next section
      element.scrollIntoView({ behavior: 'smooth' });
    }
  }
  const bringAbout = () => {
    const element = document.getElementById('about');
    if (element) {
      // 👇 Will scroll smoothly to the top of the next section
      element.scrollIntoView({ behavior: 'smooth' });
    }
  }

  const bringRoadmap = () => {
    const element = document.getElementById('roadmap');
    if (element) {
      // 👇 Will scroll smoothly to the top of the next section
      element.scrollIntoView({ behavior: 'smooth' });
    }
  }
  const gotoHome = () => {
    const element = document.getElementById('home');
    if (element) {
      // 👇 Will scroll smoothly to the top of the next section
      element.scrollIntoView({ behavior: 'smooth' });
    }
  }



  return (
    <div className={styles.link}>
        <div>
          <a data-testid="linkElement" onClick={gotoHome}>
            <p >Home</p>
          </a>
          
        </div>
        <div>
        <a   target="_self" data-testid="linkElement" onClick={bringAbout}>
          <p >About</p>
        </a>
        </div>
        <div>
        <a   target="_self" data-testid="linkElement" onClick={bringTokonomic}>
          <p >Tokenomics</p>
        </a>
            
        </div>
        <div>
        <a   target="_self" data-testid="linkElement" onClick={bringRoadmap}>
          <p >Roadmap</p>
        </a>
        </div>
        <div>
        
           
        </div>
    </div>
  )
}
