import React, {useState} from 'react'
import { GiHamburgerMenu } from "react-icons/gi";
import styles from './style.module.scss';

export default function AccountButton() {
  const [displayDropdown, setDisplayDropdown] = useState(false);
  const bringTokonomic = () => {
    const element = document.getElementById('tokenomics');
    if (element) {
    
      element.scrollIntoView({ behavior: 'smooth' });
    }
  }
  const bringAbout = () => {
    const element = document.getElementById('about');
    if (element) {
      element.scrollIntoView({ behavior: 'smooth' });
    }
  }

  const bringRoadmap = () => {
    const element = document.getElementById('roadmap');
    if (element) {
      element.scrollIntoView({ behavior: 'smooth' });
    }
  }
  const gotoHome = () => {
    const element = document.getElementById('home');
    if (element) {
      // 👇 Will scroll smoothly to the top of the next section
      element.scrollIntoView({ behavior: 'smooth' });
    }
  }
  return (
      <div  onMouseLeave={() => setDisplayDropdown( false)}>
        <GiHamburgerMenu size={30} color='white' onClick={()=>{
          setDisplayDropdown(!displayDropdown)
        }}>
        </GiHamburgerMenu>
        <div className={`${styles.dropdownContainer} ${displayDropdown ? `${styles.visible}`:null}`}>
          <div className={styles.dropdownGrid}>
            <a >
              <p onClick={() => {
                gotoHome()
                setDisplayDropdown(false)
              }}>
                Home
              </p>
            </a>
            <a >
              <p onClick={() => {
                bringAbout()
                setDisplayDropdown(false)}
              }>
                About
              </p>
            </a>
            <a >
              <p onClick={() => {
                bringTokonomic()
                setDisplayDropdown(false)
              }}>
              Tokenomics
              </p>
            </a>
            <a >
              <p onClick={() => {
                bringRoadmap()
                setDisplayDropdown(false)
              }}>
              Roadmap
              </p>
            </a>
         
          </div>
        </div>
      </div>
    
  )
}
