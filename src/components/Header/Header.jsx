import React from 'react'
import Banner from './Banner/Banner'
import AccountButton from './AccountButton/AccountButton'
import Notification from './Notification/Notification'
import style from './style.module.scss'
export default function Header() {
  return (
    <nav className={style.nav} >
      <div className={style.header_section}>
        <Banner />
        <Notification />
        <div className={style.mobile_nav_section}>
          <AccountButton />
        </div>
      </div>
    </nav>
   
  )
}
